package com.themark.ak.app.android.vkgalleryclient.features.gallery.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseActivity

class GalleryActivity : BaseActivity() {

    companion object {
        fun callingIntent(context: Context) = Intent(context, GalleryActivity::class.java)
    }

    override fun layoutId(): Int = R.layout.activity_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        savedInstanceState?: addFragment(R.id.fragmentContainer,
            GalleryFragment.newInstance()
        )
    }
}