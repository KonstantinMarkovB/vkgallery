package com.themark.ak.app.android.vkgalleryclient.features.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseViewModel
import javax.inject.Inject

class LoginViewModel
@Inject constructor(
    private val authenticator: Authenticator
) : BaseViewModel(), SimpleWebViewClient.UrlLoadingListener {

    private val _currentUri = MutableLiveData<String>()
    val currentUri: LiveData<String> = _currentUri

    private val _userLoggedIn = MutableLiveData<Boolean>()
    val userLoggedIn: LiveData<Boolean> = _userLoggedIn

    fun loadPage(){
        Log.d("TEST", "current: ${authenticator.endpoint.toString()}")
        _currentUri.value = authenticator.endpoint.toString()
    }

    override fun shouldOverrideUrlLoading(url: String): Boolean {
        if(authenticator.userLoggedIn(url)){
            _userLoggedIn.value = true
            return true
        }
        return false
    }


}