package com.themark.ak.app.android.vkgalleryclient.features.reposytory.vkimpl

import com.themark.ak.app.android.vkgalleryclient.core.di.RetrofitModule
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class VKService @Inject constructor(
    @Named(RetrofitModule.VK_API) retrofit: Retrofit
) : VKApi {

    private val vkApi by lazy { retrofit.create(VKApi::class.java) }

    override fun getAlbums(token: String, ownerId: String, version: String): Call<AlbumsEntry> =
        vkApi.getAlbums(token, ownerId, version)

    override fun getPhotos(token: String, ownerId: String, version: String, albumId: String) =
        vkApi.getPhotos(token, ownerId, version, albumId)


}