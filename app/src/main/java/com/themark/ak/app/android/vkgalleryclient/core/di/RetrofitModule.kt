package com.themark.ak.app.android.vkgalleryclient.core.di

import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class RetrofitModule {
    companion object {
        const val VK_API = "https://api.vk.com/method/"
        const val VK_AUTH = "https://oauth.vk.com/"
    }

    @Provides
    @Singleton
    @Named(VK_API)
    fun provideVkRetrofit(): Retrofit = provideRetrofit(VK_API)


    @Provides
    @Singleton
    @Named(VK_AUTH)
    fun provideVkAuthRetrofit(): Retrofit = provideRetrofit(VK_AUTH)

    private fun provideRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(createClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createClient(): OkHttpClient {
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG){
            val httpLoggingInterceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Log.d("OkHttp", message)
                }
            })
            val loggingInterceptor =
                httpLoggingInterceptor.apply { level = HttpLoggingInterceptor.Level.BASIC }

            okHttpClientBuilder.addInterceptor(loggingInterceptor)
        }
        return okHttpClientBuilder.build()
    }

}