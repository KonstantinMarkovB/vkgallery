package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.extention.failure
import com.themark.ak.app.android.vkgalleryclient.core.extention.observe
import com.themark.ak.app.android.vkgalleryclient.core.extention.viewModel
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseFragment
import com.themark.ak.app.android.vkgalleryclient.features.gallery.list.AlbumView
import kotlinx.android.synthetic.main.fragment_recycler_view.*
import javax.inject.Inject

class PhotosFragment : BaseFragment() {

    private lateinit var album: AlbumView

    @Inject lateinit var photosVM: PhotosViewModel

    @Inject lateinit var photosAdapter: PhotosAdapter

    companion object {
        private const val EXTRA_ALBUM = "com.themark.ak.app.android.vkgalleryclient.features.gallery.photos.album"

        fun newInstance(album: AlbumView): PhotosFragment =
            PhotosFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_ALBUM, album)
                }
            }
    }

    override fun layoutId(): Int = R.layout.fragment_recycler_view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        Log.d("TEST", "_________onCreate")

        album = arguments?.getParcelable(EXTRA_ALBUM)!!

        Log.d("TEST", "$album")


        photosVM = viewModel(viewModelFactory){
            observe(photos, ::handlePhotosLoaded)
            observe(photoLoaded, ::handlePhotoLoaded)
            failure(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()

        if(firstTimeCreated(savedInstanceState)){
            Log.d("TEST", "firstTimeCreated")
            photosVM.loadPhotos(album)
        }
    }

    private fun initUi(){
        activity?.let {
            albumsRV.layoutManager = GridLayoutManager(activity, 2)
        }

        photosAdapter.onClickListener = { Toast.makeText(context, "on click", Toast.LENGTH_SHORT).show() }
        photosAdapter.photoLoader = photosVM::loadPhoto

        albumsRV.adapter = photosAdapter
    }

    private fun handlePhotosLoaded(photos: List<PhotoView>?){
        photos?.let { photosAdapter.collection = photos }
    }

    private fun handlePhotoLoaded(photo: PhotoView?){
        photo?.let { photosAdapter.updateItem(photo) }
    }

    private fun handleFailure(failure: Failure?){
        Toast.makeText(context, "$failure", Toast.LENGTH_SHORT).show()
    }

}