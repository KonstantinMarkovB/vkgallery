package com.themark.ak.app.android.vkgalleryclient.features.reposytory

import android.net.Uri
import java.sql.Timestamp

class Photo(
        val id: Int,
        val albumId: Int,
        val ownerId: Int,
        val sizes: List<Size>,
        val text: String,
        val date: Timestamp,
        val likes: Int,
        val reposts: Int,
        val comments: Int
){

    class Size(
        val type: String,
        val url: Uri,
        val width: String,
        val height: String
    )

    class LikesEntry(
        val user_likes: Int,
        val count: Int
    )

    class CountEntry(val count: Int)
}