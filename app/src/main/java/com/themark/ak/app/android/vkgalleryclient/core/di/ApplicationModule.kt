package com.themark.ak.app.android.vkgalleryclient.core.di

import android.content.Context
import android.content.SharedPreferences
import android.os.Handler
import com.themark.ak.app.android.vkgalleryclient.AndroidApplication
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.AuthorisationRepository
import com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.impl.VKAuthRepo
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.PhotoRepository
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.vkimpl.VKPhotoRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(context.resources.getString(R.string.vk_preference_file_key),0)


    @Provides
    @Singleton
    fun providePhotoRepo(vkPhotoRepository: VKPhotoRepository): PhotoRepository = vkPhotoRepository

    @Provides
    @Singleton
    fun provideAuthRepo(vkAuthRepo: VKAuthRepo): AuthorisationRepository = vkAuthRepo

    @Provides
    fun privideHandler(): Handler = Handler()

}