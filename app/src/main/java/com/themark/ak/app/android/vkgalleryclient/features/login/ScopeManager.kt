package com.themark.ak.app.android.vkgalleryclient.features.login

import com.themark.ak.app.android.vkgalleryclient.R
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class ScopeManager @Inject constructor(
    @Named(R.string.scope_offline.toString()) private val scopeOffline: String,
    @Named(R.string.scope_photos.toString()) private val scopePhotos: String
) {

    fun getPhotosScoupe(): Int {
        return scopeOffline.toInt() + scopePhotos.toInt()
    }

}