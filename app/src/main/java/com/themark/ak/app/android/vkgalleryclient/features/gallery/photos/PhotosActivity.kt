package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseActivity
import com.themark.ak.app.android.vkgalleryclient.features.gallery.list.AlbumView
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album

class PhotosActivity : BaseActivity() {

    companion object {
        private const val EXTRA_ALBUM = "com.themark.ak.app.android.vkgalleryclient.features.gallery.photos.album"

        fun callingIntent(context: Context, album: AlbumView): Intent =
            Intent(context, PhotosActivity::class.java).apply {
                putExtra(EXTRA_ALBUM, album)
            }
    }

    override fun layoutId(): Int = R.layout.activity_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val album = intent.getParcelableExtra<AlbumView>(EXTRA_ALBUM)

        savedInstanceState?: addFragment(R.id.fragmentContainer,
            PhotosFragment.newInstance(album))
    }

}