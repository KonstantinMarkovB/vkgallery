package com.themark.ak.app.android.vkgalleryclient.features.gallery.list

import android.content.Context
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AlbumMapper @Inject constructor(private val context: Context) {
    fun toAlbumView(album: Album): AlbumView =
        with(album){
            AlbumView(
                id,
                owner,
                title,
                description,
                created,
                updated,
                context.resources.getQuantityString(R.plurals.numberOfPhotos, size, size),
                thumb
            )
        }
}