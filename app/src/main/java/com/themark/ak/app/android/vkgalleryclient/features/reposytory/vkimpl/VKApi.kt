package com.themark.ak.app.android.vkgalleryclient.features.reposytory.vkimpl

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface VKApi {

    companion object{
        private const val ACCESS_TOKEN = "access_token"
        private const val VERSION = "v"
        private const val OWNER_ID = "owner_id"
        private const val ALBUM_ID = "album_id"
        private const val METHOD_ALBUMS = "photos.getAlbums?need_covers=1"
        private const val METHOD_PHOTOS = "photos.get?extended=1&photo_sizes=1"
    }

    @GET(METHOD_ALBUMS) fun getAlbums(
        @Query(ACCESS_TOKEN) token: String,
        @Query(OWNER_ID) ownerId: String,
        @Query(VERSION) version: String
    ): Call<AlbumsEntry>

    @GET(METHOD_PHOTOS) fun getPhotos(
        @Query(ACCESS_TOKEN) token: String,
        @Query(OWNER_ID) ownerId: String,
        @Query(VERSION) version: String,
        @Query(ALBUM_ID) albumId: String
    ): Call<PhotosEntry>

}