package com.themark.ak.app.android.vkgalleryclient.core.navigation

import android.os.Bundle
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import com.themark.ak.app.android.vkgalleryclient.AndroidApplication
import com.themark.ak.app.android.vkgalleryclient.core.di.ApplicationComponent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class RouteActivity : AppCompatActivity() {

    private val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as AndroidApplication).appComponent
    }

    @Inject lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        GlobalScope.launch { navigator.showMain(this@RouteActivity) }.start()
    }

}