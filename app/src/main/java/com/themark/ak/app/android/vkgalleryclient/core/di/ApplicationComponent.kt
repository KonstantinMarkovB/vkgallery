package com.themark.ak.app.android.vkgalleryclient.core.di

import com.themark.ak.app.android.vkgalleryclient.core.di.viewmodel.ViewModelModule
import com.themark.ak.app.android.vkgalleryclient.AndroidApplication
import com.themark.ak.app.android.vkgalleryclient.core.navigation.RouteActivity
import com.themark.ak.app.android.vkgalleryclient.features.gallery.list.GalleryFragment
import com.themark.ak.app.android.vkgalleryclient.features.gallery.photos.PhotosFragment
import com.themark.ak.app.android.vkgalleryclient.features.login.LoginFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    RetrofitModule::class,
    ResourceModule::class,
    ViewModelModule::class
])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(routeActivity: RouteActivity)
    fun inject(loginFragment: LoginFragment)
    fun inject(galleryFragment: GalleryFragment)
    fun inject(photosFragment: PhotosFragment)
}