package com.themark.ak.app.android.vkgalleryclient.features.reposytory.vkimpl

import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure.*
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either.*
import com.themark.ak.app.android.vkgalleryclient.core.paltform.NetworkHandler
import com.themark.ak.app.android.vkgalleryclient.features.AppPreferences
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Photo
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.PhotoRepository
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class VKPhotoRepository @Inject constructor (
    private val service: VKService,
    private val networkHandler: NetworkHandler,
    private val appPreferences: AppPreferences,
    @Named(R.string.vk_api_version.toString()) private val version: String
    ): PhotoRepository {

    override fun getAlbums(owner: String): Either<Failure, List<Album>> {
        val token = appPreferences.accessToken

        return if(token != null) {
            val call = service.getAlbums(token, owner, version)

            when (networkHandler.isConnected) {
                true -> request(call, { it!!.toAlbums() }, AlbumsEntry.empty())
                false, null -> Left(NetworkConnection)
            }
        } else Left(TokenFailure())
    }

    override fun getPhotos(owner: String, albumId: String): Either<Failure, List<Photo>> {
        val token = appPreferences.accessToken
        Log.d("TEST", "getPhotos")

        return if(token != null) {
            Log.d("TEST", "token != null")

            val call = service.getPhotos(token, owner, version, albumId)

            when (networkHandler.isConnected) {
                true -> request(call, {
                    Log.d("TEST", "it != null ${it != null}")

                    it!!.toPhotos()
                }, PhotosEntry.empty())
                false, null -> Left(NetworkConnection)
            }
        } else Left(TokenFailure())
    }

    private fun <T, R> request(call: Call<T>, transform: (T?) -> R, default: T?): Either<Failure, R> {
        return try {
            val response = call.execute()
            Log.d("TEST", "response.isSuccessful = ${response.isSuccessful}" )
            when (response.isSuccessful) {
                true -> Right(transform(response.body() ?: default))
                false -> Left(ServerError)
            }
        } catch (exception: Throwable) {
            Log.e("TEST", exception.toString())
            Left(ServerError)
        }
    }
}