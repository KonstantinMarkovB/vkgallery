package com.themark.ak.app.android.vkgalleryclient.features.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.extention.observe
import com.themark.ak.app.android.vkgalleryclient.core.extention.viewModel
import com.themark.ak.app.android.vkgalleryclient.core.navigation.Navigator
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    @Inject lateinit var loginVM: LoginViewModel
    @Inject lateinit var navigator: Navigator
    lateinit var webView: WebView

    companion object {
        fun newInstance(): LoginFragment = LoginFragment()
    }

    override fun layoutId(): Int = TODO("WebView used")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        webView = WebView(context)

        loginVM = viewModel(viewModelFactory){
            observe(currentUri, ::handleCurrentUrlChanged)
            observe(userLoggedIn, ::handleUserLoggedId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        webView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()

        loginVM.loadPage()
    }

    private fun initUi(){
        webView.webViewClient = SimpleWebViewClient(loginVM)
    }

    private fun handleCurrentUrlChanged(url: String?){
        Log.d("TEST", "handleCurrentUrlChanged: $url")
        url?.let { webView.loadUrl(url) }
    }

    private fun handleUserLoggedId(isLoggedIn: Boolean?){
        if(isLoggedIn != null && context != null){
            if(isLoggedIn)
                GlobalScope.launch { navigator.showMain(context!!) }.start()
        }
    }

}


