package com.themark.ak.app.android.vkgalleryclient.features.login

import android.webkit.WebView
import android.webkit.WebViewClient

class SimpleWebViewClient(private val listener: UrlLoadingListener) : WebViewClient() {

    interface UrlLoadingListener{
        fun shouldOverrideUrlLoading(url: String): Boolean
    }

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        return if(url != null) listener.shouldOverrideUrlLoading(url)
        else false
    }
}