package com.themark.ak.app.android.vkgalleryclient.core.thread

import android.os.Handler
import android.os.HandlerThread
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

abstract class BaseHandlerThread<Target, Param, Result> constructor(
    name: String,
    private var mResponseHandler: Handler
) : HandlerThread(name) {

    private var mHasQuit = false
    private val mRequestMap: ConcurrentMap<Target, Param> = ConcurrentHashMap()
    private lateinit var mRequestHandler: Handler
    var listener: ExecuteListener<Target, Result>? = null

    companion object {
        private const val  MESSAGE_DOWNLOAD = 0
    }

    interface ExecuteListener<Target, Result> {
        fun onExecuted(target: Target, result: Result)
    }

    override fun onLooperPrepared() {
        mRequestHandler = Handler { msg ->
            if(msg.what == MESSAGE_DOWNLOAD) {
                val target = msg.obj as Target
                val param = mRequestMap[target]

                param?.let {
                    val res = handleRequest(target, param)
                    onExecuted(target, param, res)
                }

                return@Handler true
            } else {
                return@Handler false
            }
        }
    }

    fun queueThumbnail(target: Target, param: Param){
        if(param == null){
            mRequestMap.remove(target)
        } else {

            mRequestMap[target] = param
            mRequestHandler
                .obtainMessage(MESSAGE_DOWNLOAD, target)
                .sendToTarget()
        }
    }

    fun clearQueue() {
        mRequestHandler.removeMessages(MESSAGE_DOWNLOAD)
        mRequestMap.clear()
    }

    override fun quit(): Boolean {
        mHasQuit = true
        return super.quit()
    }

    private fun onExecuted(target: Target, param: Param, result: Result){
        mResponseHandler.post {
            if(!Objects.equals(mRequestMap[target], param) || mHasQuit)
                return@post
            mRequestMap.remove(target)
            listener?.onExecuted(target, result)
        }
    }

    protected abstract fun handleRequest(target: Target, param: Param): Result

}