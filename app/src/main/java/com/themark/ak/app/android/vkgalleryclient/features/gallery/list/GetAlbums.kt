package com.themark.ak.app.android.vkgalleryclient.features.gallery.list

import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either
import com.themark.ak.app.android.vkgalleryclient.core.interactor.UseCase
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.PhotoRepository
import javax.inject.Inject

class GetAlbums @Inject constructor(
    val photoRepository: PhotoRepository
) : UseCase<List<Album>, String>() {

    override suspend fun run(params: String): Either<Failure, List<Album>> =
        photoRepository.getAlbums(params)
}