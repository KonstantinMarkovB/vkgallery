package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseViewModel
import com.themark.ak.app.android.vkgalleryclient.core.thread.BaseHandlerThread
import com.themark.ak.app.android.vkgalleryclient.features.gallery.ThumbnailDownloader
import com.themark.ak.app.android.vkgalleryclient.features.gallery.list.AlbumView
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Photo
import javax.inject.Inject
import javax.inject.Named

class PhotosViewModel @Inject constructor(
    private val getPhotos: GetPhotos,
    private val photoMapper: PhotoMapper,
    private val thumbnailDownloader: ThumbnailDownloader<DownloadParams>
    ): BaseViewModel() {

    private val _photos = MutableLiveData<List<PhotoView>>()
    val photos: LiveData<List<PhotoView>> = _photos

    private val _photoLoaded = MutableLiveData<PhotoView>()
    val photoLoaded: LiveData<PhotoView> = _photoLoaded

    class DownloadParams(val photo: PhotoView, val photoSize: PhotoView.SizeView)

    init {
        thumbnailDownloader.listener = object : BaseHandlerThread.ExecuteListener<DownloadParams, Bitmap> {
            override fun onExecuted(target: DownloadParams, result: Bitmap) {
                target.photoSize.bitmap = result
                _photoLoaded.value = target.photo
            }
        }

        thumbnailDownloader.start()
        thumbnailDownloader.looper
    }

    fun loadPhotos(albumView: AlbumView){
        Log.d("TEST", "loadPhotos")
        getPhotos(GetPhotos.Params(albumView.owner, albumView.id.toString())){
            it.either(::handleFailure,::handlePhotosLoaded )
        }
    }

    fun loadPhoto(item: PhotoView, itemSize: PhotoView.SizeView){
        thumbnailDownloader.queueThumbnail(
            DownloadParams(item,  itemSize), itemSize.url.toString()
        )
    }

    private fun handlePhotosLoaded(photos: List<Photo>){
        Log.d("TEST","photos: ${photos.size}")
        _photos.value = photos.map { photoMapper.toPhotoView(it) }
    }

    override fun onCleared() {
        super.onCleared()
        thumbnailDownloader.clearQueue()
    }

}