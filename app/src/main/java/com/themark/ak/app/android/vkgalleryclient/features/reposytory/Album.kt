package com.themark.ak.app.android.vkgalleryclient.features.reposytory

import android.net.Uri
import android.os.Parcel
import com.themark.ak.app.android.vkgalleryclient.core.extention.empty
import com.themark.ak.app.android.vkgalleryclient.core.paltform.KParcelable
import com.themark.ak.app.android.vkgalleryclient.core.paltform.parcelableCreator
import java.sql.Timestamp

data class Album (
   val id: Int,
   val owner: String,
   val title: String,
   val description: String,
   val created: Timestamp,
   val updated: Timestamp,
   val size: Int,
   val thumb: Uri
) : KParcelable {
   companion object {
      @JvmField val CREATOR = parcelableCreator (::Album)
   }

   constructor(parcel: Parcel) : this(
      parcel.readInt(),
      parcel.readString() ?: String.empty(),
      parcel.readString() ?: String.empty(),
      parcel.readString() ?: String.empty(),
      Timestamp(parcel.readLong()),
      Timestamp(parcel.readLong()),
      parcel.readInt(),
      Uri.parse(parcel.readString())
   )

   override fun writeToParcel(dest: Parcel, flags: Int) {
      with(dest) {
         writeInt(id)
         writeString(owner)
         writeString(title)
         writeString(description)
         writeLong(created.time)
         writeLong(updated.time)
         writeInt(size)
         writeString(thumb.toString())
      }
   }

}