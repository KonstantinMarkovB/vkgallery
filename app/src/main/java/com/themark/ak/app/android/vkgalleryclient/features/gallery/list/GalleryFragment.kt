package com.themark.ak.app.android.vkgalleryclient.features.gallery.list

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.extention.observe
import com.themark.ak.app.android.vkgalleryclient.core.extention.viewModel
import com.themark.ak.app.android.vkgalleryclient.core.navigation.Navigator
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseFragment
import kotlinx.android.synthetic.main.fragment_recycler_view.*
import javax.inject.Inject

class GalleryFragment : BaseFragment() {

    @Inject lateinit var galleryVM: GalleryViewModel

    @Inject lateinit var adapter: AlbumAdapter

    @Inject lateinit var navigator: Navigator

    companion object {
        fun newInstance() =
            GalleryFragment()
    }

    override fun layoutId(): Int = R.layout.fragment_recycler_view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appComponent.inject(this)

        galleryVM = viewModel(viewModelFactory){
            observe(albums, ::handleAlbumsLoaded)
            observe(thumbnailLoaded, ::handleThumbnailLoaded)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()

        savedInstanceState ?: galleryVM.loadAlbums()
    }

    private fun initUi(){
        activity?.let {
            albumsRV.layoutManager = GridLayoutManager(activity, 2)
        }
        adapter.thumbnailLoader = galleryVM::loadThumbnail

        adapter.onClickListener = { album ->
            context?.let { navigator.showPhotos(it, album) }
        }
        albumsRV.adapter = adapter
    }

    private fun handleAlbumsLoaded(albums: List<AlbumView>?){
        albums?.let { adapter.collection = albums }
    }

    private fun handleThumbnailLoaded(item: AlbumView?){
        item?.let { adapter.updateItem(item) }

    }
}