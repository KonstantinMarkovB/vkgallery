package com.themark.ak.app.android.vkgalleryclient.features.reposytory.vkimpl

import android.net.Uri
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album
import java.sql.Timestamp

class AlbumsEntry(val response: Response) {

    companion object {
        fun empty(): AlbumsEntry = AlbumsEntry(
            Response(0, emptyList())
        )
    }

    fun toAlbums(): List<Album> = response.items.map { it.toAlbum() }

    class Response(val count: Int, val items: List<AlbumEntry>)

    class AlbumEntry(
        private val id: Int,
        private val thumb_id: Int,
        private val owner_id: Int,
        private val title: String,
        private val description: String,
        private val created: Long,
        private val updated: Long,
        private val size: Int,
        private val thumb_is_last: Int,
        private val thumb_src: String
    ) {
        fun toAlbum() = Album(
            id,
            owner_id.toString(),
            title,
            description,
            Timestamp(created),
            Timestamp(updated),
            size,
            Uri.parse(thumb_src)
        )
    }

}