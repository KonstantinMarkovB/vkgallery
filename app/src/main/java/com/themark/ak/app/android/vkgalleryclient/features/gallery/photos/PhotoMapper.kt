package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Photo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoMapper @Inject constructor() {

    fun toPhotoView(photo: Photo): PhotoView =
        PhotoView(
            photo.id,
            photo.albumId,
            photo.ownerId,
            toPhotoSizeView(photo.sizes[0]),
            toPhotoSizeView(photo.sizes[photo.sizes.lastIndex]),
            photo.text,
            photo.date,
            photo.likes,
            photo.reposts,
            photo.comments
        )
    private fun toPhotoSizeView(size: Photo.Size): PhotoView.SizeView =
        PhotoView.SizeView(
            size.type,
            size.url,
            size.width,
            size.height,
            null
        )

}