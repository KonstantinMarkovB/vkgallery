package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.extention.inflate
import com.themark.ak.app.android.vkgalleryclient.core.extention.invisible
import com.themark.ak.app.android.vkgalleryclient.core.extention.visible
import kotlinx.android.synthetic.main.row_photo.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class PhotosAdapter @Inject constructor(): RecyclerView.Adapter<PhotosAdapter.ViewHolder>(){
    var collection: List<PhotoView> by Delegates.observable(emptyList()) {
            _, _, _ ->
        Log.d("TEST", "NEW COLLECTIONS")
        notifyDataSetChanged()
    }

    var photoLoader: (photoView: PhotoView, sizeView: PhotoView.SizeView) -> Any = { _, _ -> }

    var onClickListener: (PhotoView) -> Any? = { _-> }

    fun updateItem(item: PhotoView){
        val pos = collection.indexOf(item)
        if(pos >= 0){
            notifyItemChanged(pos)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.row_photo))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(collection[position])

    inner class ViewHolder(private val viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        fun bind(item: PhotoView){
            Log.d("TEST", "bind  $item")
            if(item.smollSizes.bitmap != null){
                Log.d("TEST", "bind  !=null")
                viewItem.photoAnimation.invisible()
                viewItem.photoImageView.setImageBitmap(item.smollSizes.bitmap)
            } else {
                Log.d("TEST", "bind  ==null")
                viewItem.photoAnimation.visible()
                photoLoader(item, item.smollSizes)
            }
        }
    }

}