package com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.impl

import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure.*
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either.*
import com.themark.ak.app.android.vkgalleryclient.core.paltform.NetworkHandler
import com.themark.ak.app.android.vkgalleryclient.features.AppPreferences
import com.themark.ak.app.android.vkgalleryclient.features.login.ScopeManager
import com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.AuthorisationRepository
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class VKAuthRepo @Inject constructor(
    private val service: VKAuthentificationService,
    private val networkHandler: NetworkHandler,
    private val appPreferences: AppPreferences,
    @Named(R.string.vk_api_version.toString()) private val version: String,
    @Named(R.string.vk_redirect_uri.toString()) private val redirectUri: String,
    @Named(R.string.client_id.toString()) private val clientId: String,
    private val scopeManager: ScopeManager
) : AuthorisationRepository {

    override fun isAuthorised(): Either<Failure, Boolean> {
        Log.d("TEST", "VKAuthRepo ")
        return if(networkHandler.isConnected!!){
            val token = appPreferences.accessToken

            if(token != null) {
                return Right(true)
//                Log.d("TEST", "VKAuthRepo: token != null")
//                val scope = scopeManager.getPhotosScoupe().toString()
//
//                val call = service.makeQuery(clientId, redirectUri, scope, version)
//                return request(call)
            } else {
                Log.d("TEST", "VKAuthRepo: token == null")

                return Right(false)
            }
        } else {
            Log.d("TEST", "VKAuthRepo: error network")

            Left(NetworkConnection)
        }
    }

    private fun request(call: Call<Any>): Either<Failure, Boolean> {
        Log.d("TEST", "VKAuth call ${call}")
        try {
            val response = call.execute()
            when (response.isSuccessful) {
                true -> {
                    Log.d("TEST", "VKAuth ${response.headers()}")
                    return Right(false)
                }
                false -> {
                    Log.d("TEST", "VKAuth ServerError")
                    return Left(ServerError)
                }
            }
        } catch (exception: Throwable) {
            Log.e("TEST", exception.toString())
            return Left(ServerError)
        }
    }
}