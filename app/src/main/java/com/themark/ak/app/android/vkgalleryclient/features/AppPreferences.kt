package com.themark.ak.app.android.vkgalleryclient.features

import android.content.Context
import android.content.SharedPreferences
import com.themark.ak.app.android.vkgalleryclient.R
import javax.inject.Inject

class AppPreferences @Inject constructor(
    private val context: Context,
    private val preferences: SharedPreferences
) {


    var accessToken: String?
        get() = preferences.getString(getString(R.string.access_token_key), null)
        set(value) {
            preferences.edit().putString(getString(R.string.access_token_key), value).apply()
        }

    var userId: String?
        get() = preferences.getString(getString(R.string.user_id_key), null)
        set(value) {
            preferences.edit().putString(getString(R.string.user_id_key), value).apply()
        }

    private fun getString(id: Int): String = context.resources.getString(id)

}