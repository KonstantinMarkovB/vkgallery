package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import android.graphics.Bitmap
import android.net.Uri
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Photo
import java.sql.Timestamp

class PhotoView(val id: Int,
                val albumId: Int,
                val ownerId: Int,
                val smollSizes: SizeView,
                val bigSizes: SizeView,
                val text: String,
                val date: Timestamp,
                val likes: Int,
                val reposts: Int,
                val comments: Int
) {

    class SizeView(
        val type: String,
        val url: Uri,
        val width: String,
        val height: String,
        var bitmap: Bitmap?
    )
}