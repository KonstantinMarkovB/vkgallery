package com.themark.ak.app.android.vkgalleryclient.features.gallery.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.extention.inflate
import com.themark.ak.app.android.vkgalleryclient.core.extention.invisible
import com.themark.ak.app.android.vkgalleryclient.core.extention.visible
import kotlinx.android.synthetic.main.row_album.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class AlbumAdapter @Inject constructor() : RecyclerView.Adapter<AlbumAdapter.ViewHolder>() {
    var collection: List<AlbumView> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    var thumbnailLoader: (AlbumView) -> Any = { _-> }

    var onClickListener: (AlbumView) -> Any? = { _-> }

    fun updateItem(item: AlbumView){
        val pos = collection.indexOf(item)
        if(pos >= 0){
            notifyItemChanged(pos)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.row_album))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(collection[position])

    inner class ViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        fun bind(item: AlbumView){
            if(item.bitmap != null){
                itemView.thumbnail.setImageBitmap(item.bitmap)
                itemView.albumAnimation.invisible()
            } else {
                itemView.albumAnimation.visible()
                thumbnailLoader(item)
            }
            itemView.title.text = item.title
            itemView.subTitle.text = item.numberOfPhotos

            itemView.setOnClickListener { onClickListener(item) }
        }
    }
}