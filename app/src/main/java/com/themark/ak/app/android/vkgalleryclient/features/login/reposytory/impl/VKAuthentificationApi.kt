package com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.impl

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface VKAuthentificationApi {
    companion object{
        private const val ACCESS_TOKEN = "access_token"
        private const val VERSION = "v"
        private const val SCOPE = "scope"
        private const val REDIRECT_URI = "redirect_uri"
        private const val CLIENT_ID = "client_id"
    }
    //    //https://oauth.vk.com/blank.html#access_token=&expires_in=0&user_id=134582907

    @GET("/authorize?display=mobile&response_type=token&revoke=1") fun makeQuery(
        @Query(CLIENT_ID) clientId: String,
        @Query(REDIRECT_URI) redirectUri: String,
        @Query(SCOPE) scope: String,
        @Query(VERSION) version: String
    ): Call<Any>

}