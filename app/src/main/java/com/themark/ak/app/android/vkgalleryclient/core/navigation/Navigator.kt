package com.themark.ak.app.android.vkgalleryclient.core.navigation

import android.content.Context
import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either
import com.themark.ak.app.android.vkgalleryclient.features.gallery.list.AlbumView
import com.themark.ak.app.android.vkgalleryclient.features.gallery.list.GalleryActivity
import com.themark.ak.app.android.vkgalleryclient.features.gallery.photos.PhotosActivity
import com.themark.ak.app.android.vkgalleryclient.features.login.Authenticator
import com.themark.ak.app.android.vkgalleryclient.features.login.LoginActivity
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Navigator @Inject constructor(private val authenticator: Authenticator) {

    fun showMain(context: Context) {
        val res: Either<Failure, Boolean> = authenticator.userLoggedIn()
        Log.d("TEST", "showMain: $res")

        when (res){
            is Either.Right -> {
                when(res.b) {
                    true -> { Log.d("TEST", "Either.Right->true"); showGallery(context); }
                    false -> { Log.d("TEST", "Either.Right->false"); showLogin(context) }
                }
            }
            is Either.Left -> { Log.d("TEST", "Either.left");  showLogin(context) }
        }
    }

    private fun showLogin(context: Context) =
        context.startActivity(LoginActivity.callingIntent(context))

    private fun showGallery(context: Context) =
        context.startActivity(GalleryActivity.callingIntent(context))

    fun showPhotos(context: Context, album: AlbumView) =
        context.startActivity(PhotosActivity.callingIntent(context, album))
}