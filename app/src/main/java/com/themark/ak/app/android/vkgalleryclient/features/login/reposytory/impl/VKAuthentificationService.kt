package com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.impl

import com.themark.ak.app.android.vkgalleryclient.core.di.RetrofitModule
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class VKAuthentificationService @Inject constructor(
    @Named(RetrofitModule.VK_AUTH) retrofit: Retrofit
) : VKAuthentificationApi {

    private val service by lazy { retrofit.create(VKAuthentificationApi::class.java) }

    override fun makeQuery(
        clientId: String,
        redirectUri: String,
        scope: String,
        version: String
    ): Call<Any> = service.makeQuery(clientId, redirectUri, scope, version)


}