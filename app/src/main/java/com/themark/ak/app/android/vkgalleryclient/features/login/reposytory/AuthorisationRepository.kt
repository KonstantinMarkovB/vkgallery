package com.themark.ak.app.android.vkgalleryclient.features.login.reposytory

import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either

interface AuthorisationRepository {

    fun isAuthorised(): Either<Failure, Boolean>

}