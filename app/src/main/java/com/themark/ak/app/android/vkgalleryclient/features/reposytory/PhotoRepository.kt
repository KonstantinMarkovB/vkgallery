package com.themark.ak.app.android.vkgalleryclient.features.reposytory

import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either

interface PhotoRepository  {

    fun getAlbums(owner: String): Either<Failure, List<Album>>

    fun getPhotos(owner: String, albumId: String): Either<Failure, List<Photo>>
}