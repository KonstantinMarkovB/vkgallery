package com.themark.ak.app.android.vkgalleryclient.features.gallery.list

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.paltform.BaseViewModel
import com.themark.ak.app.android.vkgalleryclient.core.thread.BaseHandlerThread
import com.themark.ak.app.android.vkgalleryclient.features.gallery.ThumbnailDownloader
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Album
import javax.inject.Inject
import javax.inject.Named

class GalleryViewModel @Inject constructor(
    private val getAlbums: GetAlbums,
    private val albumMapper: AlbumMapper,
    private val thumbnailDownloader: ThumbnailDownloader<AlbumView>,
    @Named(R.string.owner_id.toString()) private val ownerId: String
) : BaseViewModel() {

    private val _albums = MutableLiveData<List<AlbumView>>()
    val albums: LiveData<List<AlbumView>> = _albums

    private val _thumbnailLoaded = MutableLiveData<AlbumView>()
    val thumbnailLoaded: LiveData<AlbumView> = _thumbnailLoaded

    init {
        thumbnailDownloader.listener = object : BaseHandlerThread.ExecuteListener<AlbumView, Bitmap> {
            override fun onExecuted(target: AlbumView, result: Bitmap) {
                target.bitmap = result
                _thumbnailLoaded.value = target
            }
        }

        thumbnailDownloader.start()
        thumbnailDownloader.looper
    }

    fun loadAlbums(){
        getAlbums(ownerId){ it.either(::handleFailure, ::handleAlbumsLoaded) }
    }

    fun loadThumbnail(item: AlbumView){
        Log.d("TEST", "load loadThumbnail")
        thumbnailDownloader.queueThumbnail(item, item.thumb.toString())
    }

    private fun handleAlbumsLoaded(albums: List<Album>){
        _albums.value = albums.map { albumMapper.toAlbumView(it) }
    }

    override fun onCleared() {
        super.onCleared()
        thumbnailDownloader.clearQueue()
    }

}