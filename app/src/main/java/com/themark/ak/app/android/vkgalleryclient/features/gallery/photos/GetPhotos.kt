package com.themark.ak.app.android.vkgalleryclient.features.gallery.photos

import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either
import com.themark.ak.app.android.vkgalleryclient.core.interactor.UseCase
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Photo
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.PhotoRepository
import javax.inject.Inject
import javax.inject.Singleton

class GetPhotos @Inject constructor(
    val photoRepository: PhotoRepository
) : UseCase<List<Photo>, GetPhotos.Params>() {
    override suspend fun run(params: Params): Either<Failure, List<Photo>> {
        Log.d("TEST", "GetPhotos $params")
        return photoRepository.getPhotos(params.owner, params.albumId)
    }


    data class Params(val owner: String, val albumId: String)
}