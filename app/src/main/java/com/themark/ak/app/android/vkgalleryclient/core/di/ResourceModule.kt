package com.themark.ak.app.android.vkgalleryclient.core.di

import android.content.Context
import com.themark.ak.app.android.vkgalleryclient.R
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ResourceModule {

    @Singleton
    @Provides
    @Named(R.string.client_id.toString())
    fun vkClientKey(context: Context): String =
        context.getString(R.string.client_id)

    @Singleton
    @Provides
    @Named(R.string.scope_offline.toString())
    fun vkScopeOffline(context: Context): String =
        context.getString(R.string.scope_offline)

    @Singleton
    @Provides
    @Named(R.string.scope_photos.toString())
    fun vkScopePhotos(context: Context): String =
        context.getString(R.string.scope_photos)

    @Singleton
    @Provides
    @Named(R.string.access_token_key.toString())
    fun ccess_token_key(context: Context): String =
        context.getString(R.string.access_token_key)

    @Singleton
    @Provides
    @Named(R.string.vk_redirect_uri.toString())
    fun vk_redirect_uri(context: Context): String =
        context.getString(R.string.vk_redirect_uri)

    @Singleton
    @Provides
    @Named(R.string.vk_api_version.toString())
    fun vk_api_version(context: Context): String =
        context.getString(R.string.vk_api_version)

    @Singleton
    @Provides
    @Named(R.string.user_id_key.toString())
    fun user_id_key(context: Context): String =
        context.getString(R.string.user_id_key)

    @Singleton
    @Provides
    @Named(R.string.owner_id.toString())
    fun owner_id(context: Context): String =
        context.getString(R.string.owner_id)
}