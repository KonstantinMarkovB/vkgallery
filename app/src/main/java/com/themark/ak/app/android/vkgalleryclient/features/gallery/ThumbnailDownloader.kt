package com.themark.ak.app.android.vkgalleryclient.features.gallery

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.core.thread.BaseHandlerThread
import java.io.BufferedInputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import javax.inject.Inject

class ThumbnailDownloader<T> @Inject constructor(
    mResponseHandler: Handler
) : BaseHandlerThread<T, String, Bitmap>("ThumbnailDownloader", mResponseHandler) {

    override fun handleRequest(target: T, param: String): Bitmap{
        val url = URL(param)

        val connection = url.openConnection() as HttpURLConnection
        connection.connect()

        try {
            if(connection.responseCode != HttpURLConnection.HTTP_OK)
                throw IOException(connection.responseMessage + ": with $param")

            val bufferedStream = BufferedInputStream(connection.inputStream)

            return BitmapFactory
                .decodeStream(bufferedStream)
        } finally {
            connection.disconnect()
        }
    }

}