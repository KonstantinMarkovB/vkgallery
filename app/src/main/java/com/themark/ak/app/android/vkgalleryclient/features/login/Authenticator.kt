package com.themark.ak.app.android.vkgalleryclient.features.login

import android.net.Uri
import android.util.Log
import com.themark.ak.app.android.vkgalleryclient.R
import com.themark.ak.app.android.vkgalleryclient.core.exception.Failure
import com.themark.ak.app.android.vkgalleryclient.core.functional.Either
import com.themark.ak.app.android.vkgalleryclient.features.AppPreferences
import com.themark.ak.app.android.vkgalleryclient.features.login.reposytory.AuthorisationRepository
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class Authenticator
@Inject constructor(
    @Named(R.string.client_id.toString()) private val clientId: String,
    @Named(R.string.access_token_key.toString()) private val accessTokenKey: String,
    @Named(R.string.user_id_key.toString()) private val userIdKey: String,
    @Named(R.string.vk_redirect_uri.toString()) private val redirectUri: String,
    @Named(R.string.vk_api_version.toString()) private val version: String,
    scopeManager: ScopeManager,
    private val appPreferences: AppPreferences,
    private val repository: AuthorisationRepository
){

    val endpoint: Uri = Uri
        .parse("https://oauth.vk.com/authorize?")
        .buildUpon()
        .appendQueryParameter("response_type", "token")
        .appendQueryParameter("display", "mobile")
        .appendQueryParameter("scope", scopeManager.getPhotosScoupe().toString())
        .appendQueryParameter("v", version)
        .appendQueryParameter("redirect_uri", redirectUri)
        .appendQueryParameter("client_id", clientId)
        .build()

    fun userLoggedIn(): Either<Failure, Boolean> {
        return repository.isAuthorised()
    }

    // https://oauth.vk.com/blank.html#access_token=c19998ec6a168bf8a5c53f99a304b8f27510cced1d33d96752ba5d9e72a99ee679a9a1563b9dae9189e77&expires_in=86400&user_id=134582907
    fun userLoggedIn(stringUrl: String): Boolean {
        val url = Uri.parse(stringUrl.replaceFirst("#","?"))

        val accessToken: String? = url.getQueryParameter(accessTokenKey)
        val userId: String? = url.getQueryParameter(userIdKey)

        Log.d("TEST", "userLoggedIn: $url")
        Log.d("TEST" ,"userLoggedIn: $accessTokenKey=$accessToken    $userIdKey=$userId")

        if(accessToken != null && userId !=  null){
            appPreferences.accessToken = accessToken
            appPreferences.userId = userId

            return true
        }

        return false
    }

}
