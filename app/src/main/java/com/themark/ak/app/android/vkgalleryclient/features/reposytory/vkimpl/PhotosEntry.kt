package com.themark.ak.app.android.vkgalleryclient.features.reposytory.vkimpl

import android.net.Uri
import com.themark.ak.app.android.vkgalleryclient.features.reposytory.Photo
import java.sql.Timestamp

class PhotosEntry(
    private val response: ResponseEntry
) {
    companion object {
        fun empty() = PhotosEntry(ResponseEntry(0, emptyList()))
    }

    fun toPhotos(): List<Photo> = response.items.map { it.toPhoto() }

    class ResponseEntry(
        val count: Int,
        val items: List<ItemEntry>
    )

    class ItemEntry(
        val id: Int,
        val album_id: Int,
        val owner_id: Int,
        val sizes: List<SizeEntry>,
        val text: String,
        val date: Long,
        val likes: LikesEntry,
        val reposts: CountEntry,
        val comments: CountEntry,
        val can_comment: Int,
        val tags: CountEntry
    ) {
        fun toPhoto(): Photo = Photo(
            id,
            album_id,
            owner_id,
            sizes.map { it.toSize() },
            text,
            Timestamp(date),
            likes.count,
            reposts.count,
            comments.count
        )
    }

    class SizeEntry(
        val type: String,
        val url: String,
        val width: String,
        val height: String
    ) {
        fun toSize(): Photo.Size = Photo.Size(
            type, Uri.parse(url), width, height
        )
    }


    class LikesEntry(
        val user_likes: Int,
        val count: Int
    )

    class CountEntry(val count: Int)

}